#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include "SPI.h"
#include "case.h"
#include "cpu.h"
#include "datetimeHandler.h"
#include "gfx_1.h"
#include "gpu.h"
#include <ESP8266WiFi.h>
#include <XPT2046_Touchscreen.h>

#define CS_PIN D0
#define TFT_DC D4
#define TFT_CS D2
#define TFT_RESET 10

#define DHTPIN D4
#define DHTTYPE DHT22

XPT2046_Touchscreen ts(CS_PIN);
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#include <EEPROM.h>
#include <Firmata.h>
#include <SerialPacket.h>

static uint32_t tempTime = 0;
static uint32_t rtcTime = 0;
static uint32_t displayTime = 0;
static String serialIntput, response;

#ifdef SERIAL_DEBUG
#define DEBUG_BEGIN(baud)                                                      \
  Serial.begin(baud);                                                          \
  while (!Serial)                                                              \
  {                                                                            \
    ;                                                                          \
  }
#define DEBUG_PRINTLN(x)                                                       \
  Serial.println(x);                                                           \
  Serial.flush()
#define DEBUG_PRINT(x) Serial.print(x)
#else
#define DEBUG_BEGIN(baud)
#define DEBUG_PRINTLN(x)
#define DEBUG_PRINT(x)
#endif

#define SSID_SIZE 32
#define PASS_SIZE 64

char ssid[SSID_SIZE + 1];
char password[PASS_SIZE + 1];
uint32_t remote_ip;
uint16_t remote_port;

void readROM(char *buf, int romIndex, int size)
{
  for (int i = 0; i < size; ++i)
  {
    buf[i] = char(EEPROM.read(i + romIndex));
  }
  buf[size] = 0;
}

void writeROM(char *buf, int romIndex, int size)
{
  for (int i = 0; i < size; ++i)
  {
    EEPROM.write(i + romIndex, buf[i]);
  }
}

void printWifiStatus()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("WiFi connected");
  }
  else
  {
    Serial.println("WiFi not connected");
    Serial.println(
        "Only serial interface will be available to setup AP settings");
  }
  Serial.flush();
}

void executeSerialCommand(String command)
{
  DEBUG_PRINT("Executing command:");
  DEBUG_PRINTLN(command);
  EEPROM.begin(128);
  if (command.indexOf("set ssid ") == 0)
  {
    String s = command.substring(9);
    s.toCharArray(ssid, SSID_SIZE);
    ssid[s.length()] = 0;
    writeROM(ssid, 0, SSID_SIZE);
    Serial.print("ssid was set:");
    Serial.println(s);
  }
  else
  {
    if (command.indexOf("set pass ") == 0)
    {
      String s = command.substring(9);
      s.toCharArray(password, PASS_SIZE);
      password[s.length()] = 0;
      writeROM(password, SSID_SIZE, PASS_SIZE);
      Serial.println("password was set.");
    }
    else
    {
      if (command.indexOf("set remote ip ") == 0)
      {
        String s = command.substring(14);
        char *ipAddr = (char *)&remote_ip;
        int offset = 0;
        for (int i = 0; i < 4; i++)
        {
          int idx = s.indexOf('.', offset);
          if (idx > 0)
          {
            String num = s.substring(offset, idx);
            ipAddr[i] = num.toInt();
            offset = idx + 1;
          }
          else
          {
            String num = s.substring(offset);
            ipAddr[i] = num.toInt();
            break;
          }
        }
        EEPROM.put(SSID_SIZE + PASS_SIZE, remote_ip);
        Serial.println("remote_ip was set.");
      }
      else
      {
        if (command.indexOf("set remote port ") == 0)
        {
          String s = command.substring(16);
          remote_port = s.toInt();
          EEPROM.put(SSID_SIZE + PASS_SIZE + sizeof(remote_ip), remote_port);
          Serial.print("remote_port was set:");
          Serial.println(s);
        }
        else
        {
          if (command.indexOf("connect") == 0)
          {
            // delete stream;
            // stream = new EthernetClientStream(client, IPAddress(0, 0, 0, 0),
            //                                   IPAddress(remote_ip), NULL,
            //                                   remote_port);
            WiFi.begin(ssid, password);
            Serial.println("Connecting to Wifi...");
          }
          else
          {
            if (command.indexOf("status") == 0)
            {
              printWifiStatus();
            }
          }
        }
      }
    }
  }
  EEPROM.commit();
  EEPROM.end();
  Serial.flush();
}

#include <functional>

void findStoreData(String buffer, String command,
                   std::function<int8_t(String, String &)> updateValue)
{
  String response;
  if (buffer.indexOf(command) == 0)
  {
    String s = buffer.substring(16);
    Serial.print("OK\n");
    updateValue(s, response);
  }
}

void getPcData(String buffer)
{
  findStoreData(buffer, "cpu_usage", setCpuUsage);
  findStoreData(buffer, "cpu_temp", setCpuTemp);
  findStoreData(buffer, "cpu_fan", setCpuFan);

  findStoreData(buffer, "gpu_temp", setGpuTemp);
  findStoreData(buffer, "gpu_fan", setGpuFan);

  findStoreData(buffer, "mem_ram", setRamUsage);
  findStoreData(buffer, "mem_hdd", setDiskUsage);

  findStoreData(buffer, "case_temp", setCaseTemp);
  findStoreData(buffer, "case_fan", setCaseFan);

  findStoreData(buffer, "net_in", setEthDown);
  findStoreData(buffer, "net_out", setEthUp);

  findStoreData(buffer, "time", setTime);
}

boolean wastouched = true;

void initHardware()
{
  Serial.begin(115200);
  delay(100);
  pinMode(TFT_RESET, OUTPUT);
  digitalWrite(TFT_RESET, LOW);
  delay(100);
  digitalWrite(TFT_RESET, HIGH);
  delay(100);
}

void setup()
{
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepBegin();
  delay(1);

  initHardware();

  tft.begin();
  tft.setRotation(3);
  ts.begin();
  initCpuParameters();
  gfx1_layout();

  Serial.write("init finished");
}

void loop()
{
  uint32_t currentTime = millis();
  if (currentTime > displayTime)
  {
    displayTime = currentTime + 500;
    gfx1_refresh = true;
    gfx1_udpate();
  }

  if (Serial.available())
  {
    getPcData(Serial.readStringUntil('\n'));
  }
}

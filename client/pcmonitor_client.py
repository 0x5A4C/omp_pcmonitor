import sys
import serial
import psutil
from psutil._common import bytes2human
import time
from datetime import datetime

serial_com = None

def send_request(com, request):
    print(request)
    com.write(request.encode())
    com.flush()
    count = 100

    while count > 0:
        response = com.readline()
        confirmation = False

        try:
            confirmation = (response.decode('utf8').strip().find('OK') != -1)
        except:
            print("error in decode")

        if confirmation:
            print("OK")
            response = com.readline()
            break
        response = com.readline()
        print("response: {0}".format(response))
        time.sleep(0.01)
        count -= 1

    com.readlines()
    com.flush()

def send_command_str(com, command, str):
    request = "{0}{1}\n".format(command.ljust(16), str)
    send_request(com, request)

def send_command_value(com, command, value):
    # request = "{0: <16}{1}".format(command, value)
    request = "{0}{1}\n".format(command.ljust(16), value)
    send_request(com, request)

def send_cpu_usage(com):
    cpu_pct = psutil.cpu_percent(interval=None, percpu=False)
    send_command_value(com, "cpu_usage:", int(cpu_pct))

def send_cpu_temp(com):
    cpu_temp = 0
    if hasattr(psutil, "sensors_temperatures"):
        temps = psutil.sensors_temperatures()
        cpu_temp = temps['coretemp'][0].current

    send_command_value(com, "cpu_temp:", int(cpu_temp))

def send_cpu_fan(com):
    cpu_fan = 0
    send_command_value(com, "cpu_fan:", int(cpu_fan))

def send_gpu_temp(com):
    gpu_temp = 0
    if hasattr(psutil, "sensors_temperatures"):
        temps = psutil.sensors_temperatures()
        gpu_temp = temps['pch_cannonlake'][0].current

    send_command_value(com, "gpu_temp:", int(gpu_temp))

def send_gpu_fan(com):
    gpu_fan = 0
    send_command_value(com, "gpu_fan:", int(gpu_fan))

def send_mem_ram(com):
    mem_ram = bytes2human(psutil.virtual_memory()[1])[:-3]
    send_command_value(com, "mem_ram:", int(mem_ram))

def send_mem_hdd(com):
    mem_hdd = bytes2human(psutil.disk_usage("/").used)[:-3]
    send_command_value(com, "mem_hdd:", int(mem_hdd))

def send_case_temp(com):
    case_temp = 0
    if hasattr(psutil, "sensors_temperatures"):
        temps = psutil.sensors_temperatures()
        case_temp = temps['acpitz'][0].current

    send_command_value(com, "case_temp:", int(case_temp))

def send_case_fan(com):
    case_fan = 0
    send_command_value(com, "case_fan:", int(case_fan))


last_time_c = None
last_time_rx = None
last_time_tx = None
def bandwith():
    global last_time_c, last_time_rx, last_time_tx
    if last_time_c is None:
        last_time_c = datetime.now()
        tmp = psutil.net_io_counters(pernic=True)['eno1']
        last_time_rx = tmp.bytes_recv
        last_time_tx = tmp.bytes_sent

    new_time = datetime.now()
    diff = (new_time - last_time_c)
    delta = (diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 1000)
    if delta >= 1000.: #ms
        eth0 = psutil.net_io_counters(pernic=True)['eno1']
        kb_tx = (eth0.bytes_sent - last_time_tx) / delta;
        kb_rx = (eth0.bytes_recv - last_time_rx) / delta;
        last_time_c = new_time
        last_time_rx = eth0.bytes_recv
        last_time_tx = eth0.bytes_sent
        return kb_tx, kb_rx
    else:
        return None, None

def send_net(com):
    net_out, net_in = bandwith()
    if net_out is not None:
        send_command_value(com, "net_out:", int(net_out))
    if net_in is not None:
        send_command_value(com, "net_in:", int(net_in))

last_time = ""
def send_time(com):
    global  last_time
    time = datetime.now().strftime('%H:%M:%S')
    if last_time != time:
        last_time = time
        send_command_str(com, "time", time)

def send_pc_data(com):
    send_cpu_usage(com)
    send_cpu_temp(com)
    send_cpu_fan(com)

    send_gpu_temp(com)
    send_gpu_fan(com)

    send_mem_ram(com)
    send_mem_hdd(com)

    send_case_temp(com)
    send_case_fan(com)

    send_net(com)

    send_time(com)

def main( argv):
    global serial_com
    serial_com = serial.Serial('/dev/ttyUSB0', 115200, timeout=0)
    serial_com.close()
    serial_com.open()

    while 1:
        send_pc_data(serial_com)
        time.sleep(0.5)
        # time.sleep(1)

if __name__ == "__main__":
    main(sys.argv[1:])
